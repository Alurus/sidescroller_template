-------------------------------------------------------------------------------
--
-- <scene>.lua
--
-------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )

-------------------------------------------------------------------------------
local goToCredits

function scene:create( event )
    local sceneGroup = self.view

    -- Called when the scene's view does not exist
    -- 
    -- INSERT code here to initialize the scene
    -- e.g. add display objects to 'sceneGroup', add touch listeners, etc
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then
     
    elseif phase == "did" then




        goToCredits = self:getObjectByName( "credits" )
        if goToCredits then
            -- touch listener for the button
            function goToCredits:touch ( event )
                local phase = event.phase
                if "ended" == phase then
                    composer.gotoScene( "gameCredits", { effect = "fade", time = 300 } )
                end
            end
            -- add the touch event listener to the button
            goToCredits:addEventListener( "touch", goToCredits )
        end


        
        World_1 = self:getObjectByName("W1")
        if World_1 then

            function World_1:touch(event)
                local phase = event.phase
                if "ended" == phase then
                    -- composer.loadScene( "level", false, options )
                    composer.gotoScene( "level", { effect = "fade", time = 300 } )
                end
            end
            World_1:addEventListener( "touch", World_1 )
        end

        SettingsButton = self:getObjectByName("SettingsButton")
        if SettingsButton then

            function SettingsButton:touch(event)
                local phase = event.phase
                if "ended" == phase then
                   
                    composer.gotoScene( "settings", { effect = "fade", time = 300 } )
                end
            end
            SettingsButton:addEventListener( "touch", SettingsButton )
        end



        TutorialButton = self:getObjectByName("TutorialButton")
        if TutorialButton then

            function TutorialButton:touch(event)
                local phase = event.phase
                if "ended" == phase then
                   
                    composer.gotoScene( "gameTutorial", { effect = "fade", time = 300 } )
                end
            end
            TutorialButton:addEventListener( "touch", TutorialButton )
        end

    end 
end






function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

-------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-------------------------------------------------------------------------------

return scene
