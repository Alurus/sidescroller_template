---------------------------------------------------------------------------------
--
-- main.lua
--
---------------------------------------------------------------------------------

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- require the composer library
local composer = require "composer"
-- physics.start( )
-- physics.setDrawMode( "hybrid" )
-- load scene1
composer.gotoScene( "loading" )

-- Add any objects that should appear on all scenes below (e.g. tab bar, hud, etc)

