-------------------------------------------------------------------------------
--
-- <scene>.lua
--
-------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )
-- Define reference points locations anchor ponts
local TOP_REF = 0
local BOTTOM_REF = 1
local LEFT_REF = 0
local RIGHT_REF = 1
local CENTER_REF = 0.5
-------------------------------------------------------------------------------
    local function backToMenu()

    end
    --Enemy Functions--        --Button to return to level Selection 
    local exit = false
    local speed = 0
       local counter =0
-------------------------------------------------------------------------------
function scene:create( event )
    local sceneGroup = self.view

    local exit = false
    counter = counter + 1
    print("Count",counter)











end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase

    if phase == "will" then

        local gameOver = self:getObjectByName("GO")
        local LeftButton = self:getObjectByName("Left")
        local RightButton = self:getObjectByName("Right")

        local player = self:getObjectByName("Player")
        local bullet = self:getObjectByName("Bullet")

        local Wall1 = self:getObjectByName("Wall1")
        local Wall2 = self:getObjectByName("Wall2")

        local road = self:getObjectByName("Road")
        local road2 = self:getObjectByName("Road_2")    
        local tree = self:getObjectByName("Tree")        


        --Player Attributes--
        -- player.gravityScale = 0
        player.isFixedRotation = true
        player.isSensor = true -- So bullet doesn't hit player car
        player.myName = "Player"

        --Bullet Attributes -- 
        bullet.isBullet = true
        bullet.isVisible = false     --The bullet will be invisible until we fire it--
        bullet.isFixedRotation = true
        bullet.myName = "Bullet"

        gameOver.isVisible = false


        local enemy = self:getObjectByName("Enemy")
        enemy.isVisible = false


        local function spawnEnemy( ... )

            --Enemy Attributes--
          
            enemy.gravityScale = 0
            enemy.isFixedRotation = true
            enemy.isSensor = true
            enemy.isVisible = true
            enemy.myName = "EnemyName"
            enemy.x = 271
            enemy.y = -46.5 
            enemy:setLinearVelocity(0,102)

            return enemy
        end

        --Spawn Initial Enemy--
        local enemy = spawnEnemy() 

        local function enemyCollisions()
              
            --If enemy is dead,spawn a new one--
                if enemyAlive == false then
                    print("Spawing")
                    local enemy = spawnEnemy()      
                    enemyAlive= true              
                end
              

            --If enemy goes off the screen,reposition it--
            if enemy.y > 480 then
                enemy.y = -46.5 
            end


            local function playerCollisionListener( self, event )
                local other = event.other.myName

                if ( event.phase == "began" ) then
           
                    if other == "EnemyName"  then
   
                        enemy.isVisible = false
             
                        local function setFalse()
                      
                         enemyAlive = false  
                        end
                        --Respawn Enemy After 600ms
                        timer.performWithDelay( 950, setFalse ,1 )
                    end
                                              
                elseif ( event.phase == "ended" ) then
                                              
                end
            end
                
            player.collision = playerCollisionListener
            player:addEventListener( "collision", player )




            --Collision Listener For bullet,if the bullet his the enemy player he will die.
            local function bulletListner( self, event )
                local other = event.other.myName
                if ( event.phase == "began" ) then
                     
                    if other == "EnemyName" then
                       
                        enemy.isVisible = false
                        bullet.isVisible =false

                        local function setFalse()
                          
                            enemyAlive = false                        
                        end
                          --Respawn Enemy After 950ms
                        timer.performWithDelay( 950, setFalse ,1 )



                    end
                                              
                elseif ( event.phase == "ended" ) then
                                              
                end
            end
                
            bullet.collision = bulletListner
            bullet:addEventListener( "collision", bullet )
        end


        Runtime:addEventListener("enterFrame",enemyCollisions)


        --Moves the roads giving the illusion of a perpetual road--
        local function moveRoads(event)
            --move Road--
            speed = 1.0

            --Moves the road & tree--
            road.y = road.y + speed
            road2.y = road2.y + speed
            tree.y = tree.y + speed
            --The following if statements move the objects back to the top of the screen once they have gone off the screen
            if (road.y + road.contentHeight)> 795 then
                
                road.y = -160 

            end
            if (road2.y + road2.contentHeight) > 795 then
                
                road2.y = -160

            end
            if tree.y  > 480 then
                
                tree.y = -160

            end
        end     
        Runtime:addEventListener( "enterFrame", moveRoads )



        -------------------------------------------------------------------------------
        --Buttons--
        -------------------------------------------------------------------------------
        local function mouseShoot( event )
            if (event.phase == "began") then
                
                --You can only shoot within these boundaraies ,So the player doesn't fire when clicking buttons--
                if event.x>100 and event.x<400 then
                    --Position the bullet--
                    bullet.x = player.x
                    bullet.y = player.y-24

                    bullet.isSensor = false
                    bullet.isVisible = true
                    local speed =700
                    --Distance between mouse event & Player --
                    deltaX = event.x - player.x
                    deltaY = event.y - player.y

                    normDeltaX = deltaX / math.sqrt(math.pow(deltaX,2) + math.pow(deltaY,2))
                    normDeltaY = deltaY / math.sqrt(math.pow(deltaX,2) + math.pow(deltaY,2))

                    bullet:setLinearVelocity( normDeltaX  * speed, normDeltaY  * speed )

                    
                end
            end
        end
        Runtime:addEventListener( "touch", mouseShoot )

        function player:left()
            --Moves the player to the left lane--
            transition.to( player, { time=250, x=206, transition=easing.inExpo } )
        end

        function player:right()
            --Moves the player to the right lane--
            transition.to( player, { time=250, x=275, transition=easing.linear} )
        end

        function LeftButton:touch(event)
            if event.phase == "ended" then
                player:left()

                return true
            end
        end
        LeftButton:addEventListener( "touch", LeftButton)

        function RightButton:touch(event)
            if event.phase == "ended" then
                player:right() 
                return true
            end
        end
        RightButton:addEventListener( "touch", RightButton)




        pauseMenu = self:getObjectByName( "backButton" )
        
        function pauseMenu:touch ( event )
            
            local phase = event.phase
            if "ended" == phase then

                
                --It is good practice to remove event listeners when exiting the scene-- 
                enemy:setLinearVelocity(0,0)                                   
                Runtime:removeEventListener("enterFrame",enemyCollisions)                   
                Runtime:removeEventListener( "enterFrame", moveRoads )
                RightButton:removeEventListener( "touch", RightButton)
                LeftButton:removeEventListener( "touch", LeftButton)  
                -- pauseMenu:removeEventListener( "touch", pauseMenu )  
              

                local options =
                    {
                    isModal = true,
                    effect = "fade",
                    time = 400,
                    params = {}                     
                    }
                    composer.showOverlay( "pause" ,options )
                end
        end
        -- add the touch event listener to the button
        pauseMenu:addEventListener( "touch", pauseMenu )

        function scene:resumeGame()

                enemy:setLinearVelocity(0,102)
                Runtime:addEventListener("enterFrame",enemyCollisions)                   
                Runtime:addEventListener( "enterFrame", moveRoads )
                RightButton:addEventListener( "touch", RightButton)
                LeftButton:addEventListener( "touch", LeftButton) 
                -- pauseMenu:addEventListener( "touch", pauseMenu )         
        end


    elseif phase == "did" then




    end 
end

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
       -- composer.removeScene( "level", false ) --
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
end


function scene:destroy( event )
    local sceneGroup = self.view
    print("working")

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

-------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-------------------------------------------------------------------------------

return scene
