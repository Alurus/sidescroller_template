-------------------------------------------------------------------------------
--
-- <scene>.lua
--
-------------------------------------------------------------------------------

local sceneName = ...

local composer = require( "composer" )

-- Load scene with same root filename as this file
local scene = composer.newScene( sceneName )
local resumeGame = false
-------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view


    local ResumeButton = self:getObjectByName("ResumeB")
    local MenuButton = self:getObjectByName("MenuB")
    local RestartButton = self:getObjectByName("RestartB")


    function ResumeButton:touch(event)
        if event.phase == "began" then
            resumeGame = true             
            composer.hideOverlay("fade",400)


        elseif event.phase == "ended" then 
          

        end
        return true
    end
    ResumeButton:addEventListener( "touch", ResumeButton)


    function MenuButton:touch(event)
        if event.phase == "began" then
                              
            composer.hideOverlay("fade",400)
            composer.removeScene( "level", false )      
            composer.gotoScene( "worldMenu", { effect = "slideUp", time = 120 } )         
        elseif event.phase == "ended" then 
          

        end
        return true
    end
    MenuButton:addEventListener( "touch", MenuButton)
   

    function RestartButton:touch(event)
        if event.phase == "began" then


            composer.removeScene( "level", true )                          
            composer.gotoScene( "level", { effect = "fade", time = 500 } )         
        elseif event.phase == "ended" then 
          

        end
        return true
    end
    RestartButton:addEventListener( "touch", RestartButton)


end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    

    if phase == "will" then
    
  
    elseif phase == "did" then
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc
    end 
end

function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase
    local parent = event.parent

    if event.phase == "will" then

            parent:resumeGame()   
            



    elseif phase == "did" then
        -- Called when the scene is now off screen
    end 
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

-------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-------------------------------------------------------------------------------

return scene
